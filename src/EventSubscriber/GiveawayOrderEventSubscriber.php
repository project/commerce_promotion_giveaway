<?php

namespace Drupal\commerce_promotion_giveaway\EventSubscriber;

use Drupal\commerce_promotion\Entity\PromotionInterface;
use Drupal\commerce_promotion\PromotionUsageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;

class GiveawayOrderEventSubscriber implements EventSubscriberInterface {

  /**
   * The promotion storage.
   *
   * @var \Drupal\commerce_promotion\PromotionStorageInterface
   */
  protected $promotionStorage;

  /**
   * The promotion usage.
   *
   * @var \Drupal\commerce_promotion\PromotionUsageInterface
   */
  protected $usage;

  /**
   * Constructs a new GiveawayOrderEventSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_promotion\PromotionUsageInterface $usage
   *   The promotion usage.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, PromotionUsageInterface $usage) {
    $this->promotionStorage = $entity_type_manager->getStorage('commerce_promotion');
    $this->usage = $usage;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [
      'commerce_order.place.pre_transition' => 'registerUsage',
    ];
    return $events;
  }

  /**
   * Registers promotion usage when the order is placed.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The workflow transition event.
   */
  public function registerUsage(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    foreach ($order->getItems() as $item) {
      $promotion_id = $item->getData('giveaway_not_adjusted');
      if ($promotion_id) {
        $promotion = $this->promotionStorage->load($promotion_id);
        if ($promotion instanceof PromotionInterface) {
          $this->usage->register($order, $promotion);
        }
      }
    }
  }

}
